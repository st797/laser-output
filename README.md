# Laser Array Generator 

## Description
This script is intended for users scanning samples at CHESS with a laser. A user-friendly command line interface takes the laser parameters and generates an output file. The file is formatted to be understood by SPEC (the beamline control software).  The laser is fixed so motors are used to translate and rotate the sample for mapping. Every sample is unique and requires its own array. This Python script is used to generate motor arrays to capture how the user's sample is sitting in space in an efficient way.

## Visuals
Save As GUI window with a plot of points to be scanned by the laser. The **Save Front Plate** button will save the front *(omega)* side of the sample. The **Save Back Plate** button will save back (_omega+180_) side of the sample. 
![Plot of points to be scanned and save as GUI](plot.jpg)

## Installation
Clone Laser Output Repository. <br />
Create a conda environment from the environment.yml file. <br />
Activate the conda environment. <br />
Run script. 

## Usage
The output of the script is a text file of the motor arrays. Users can save the file for the front and/or back side (+180 deg) of the sample. This file is formatted to be used in SPEC as motor position instructions. 

To access the help menu for the required inputs for this script, the command line input is <br />
**python LaserOutput.py -h**

Command Line Input: <br />
**python LaserOutput.py --order xyz --x 1 --y 50 --z 100 --ome 0 --nx 5 --ny 10 --nz 20 --Relx -1 5 --Rely -1 5 --Relz -1 --CTTime .2** <br />

	 	Example of front side file 
1 0.000000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
2 1.200000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
3 2.400000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
4 3.600000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
5 4.800000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
6 6.000000 49.000000 101.000000 0.000000 999.000000 0.200000 <br />
7 0.000000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
8 1.200000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
9 2.400000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
10 3.600000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
11 4.800000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
12 6.000000 49.600000 101.000000 0.000000 999.000000 0.200000 <br />
13 0.000000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
14 1.200000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
15 2.400000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
16 3.600000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
17 4.800000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
18 6.000000 50.200000 101.000000 0.000000 999.000000 0.200000 <br />
19 0.000000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
20 1.200000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
21 2.400000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
22 3.600000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
23 4.800000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
24 6.000000 50.800000 101.000000 0.000000 999.000000 0.200000 <br />
25 0.000000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
26 1.200000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
27 2.400000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
28 3.600000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
29 4.800000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
30 6.000000 51.400000 101.000000 0.000000 999.000000 0.200000 <br />
31 0.000000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
32 1.200000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
33 2.400000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
34 3.600000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
35 4.800000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
36 6.000000 52.000000 101.000000 0.000000 999.000000 0.200000 <br />
37 0.000000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />
38 1.200000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />
39 2.400000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />
40 3.600000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />
41 4.800000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />
42 6.000000 52.600000 101.000000 0.000000 999.000000 0.200000 <br />

Col 0 = Data point number <br />
Col 1 = X-Position <br />
Col 2 = Y-Position <br />
Col 3 = Z-Position <br />
Col 4 = Omega (nominal point is perpendicular to laser) <br />
Col 5 = Flag for SPEC to ignore column <br />
Col 6 = Dwell time (s) <br />

## Support
For help with this script reach out to Peter Ko, Kelly Nygren, Keara Soloway, or Rolf Verberg.

## Authors and acknowledgment
Author: Sophie Turner <br />
Thank you to my mentors Keara Soloway, Peter Ko, Rolf Verberg, and Kelly Nygren for assisting me through this project.

## Project status
This project is currently only for rectangular and linear grids. Further development can allow for irregular geometries and/or irregular intervals.
