#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 10:59:51 2022

@author: Sophie Turner
"""

# used to perform the laser scan for SPEC
# =============================================================================
# GENERATE LASER PTS --- X,Y,Z Direction
# =============================================================================

import os
from tkinter import *
from tkinter.filedialog import asksaveasfilename
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import argparse
import readline
import sys


def OuputForSpec2R(file, XYZ, CtTime, R1, R2): #sets format for array, sets up what you wanna record 
    """ 
    This fuction takes the inputs and writes them as column vectors into the specified file.
        inputs: file = file name
               XYZ = array with the X, Y, and Z values as column vectors. 
               CtTime = Count time is the scan time intervals
               R1 = omega value 
               R2 = Flag for SPEC to ignore column. 
    """
    if np.isscalar(CtTime):
        CtTime = np.ones(XYZ.shape[0]) * CtTime
        
    if np.isscalar(R1):
        R1 = np.ones(XYZ.shape[0]) * R1
        
    if np.isscalar(R2):
        R2 = np.ones(XYZ.shape[0]) * R2
        
    # File Writing 
    with open(file, 'w') as file1:
        for ii in range(XYZ.shape[0]):
            file1.write( "%s %f %f %f %f %f %f \n" % ((ii+1), XYZ[ii,0], XYZ[ii,1], XYZ[ii,2], R1[ii], R2[ii], CtTime[ii])) #s,m,f


def dir_path(path):
    """ Turns the output_path argparse input into a directory path """
    if os.path.isdir(path):
        return path
    else:
        return False 
    
    
# Creatig a save as gui for saving front side
def front_gui(): 
    """ Creating a GUI window for the front side of the plate """
    win = Tk() #create tinker window for front plate
    win.geometry("250x200") #set the geometry of the tkinter window

    win.title("Front Plate")
    btn = Button(win, text ="Save", command=save_file_f)
    btn.pack(pady=50)  
     
    win.mainloop()
    
    
# Creating a savas gui for saving back side    
def back_gui():
    """ Creating a GUI window for the back side of the plate """
    win = Tk() #create tinker window for back plate
    win.geometry("250x200") #set the geometry of the tkinter window

    win.title("Back Plate")
    btn = Button(win, text ="Save", command=save_file_b)
    btn.pack(pady=50)  
     
    win.mainloop()

# =============================================================================
# SETUP - USER INPUT
# =============================================================================
# The laser alignment is done at 0 as the primary, +180 as secondary

print("Please Input the Desired Parameters. Enter -h for help")

parser = argparse.ArgumentParser(description=('===== Laser Scan Parameters \n *Note:In Chess reference frame ====='))
parser.add_argument('--order', type=str, help='Scan axis order (name of fastest scanned dimension first, slowest dimension last)', 
                    choices=('xyz', 'yzx', 'zxy', 'xzy', 'zyx', 'yxz'))
parser.add_argument( '--x', type=float , help='Nominal center of the X range, must be float', metavar='sampXcp', required = True )
parser.add_argument('--y', type=float, help='Nominal center of the Y range, must be float', metavar='sampYcp', required = True)
parser.add_argument('--z', type=float, help = 'Nominal center of the Z range, must be float', metavar='rsampZ', required=True )
parser.add_argument('--ome', type=float, help='Omega value, must be float', required = True)
parser.add_argument('--nx', type=int, help='Number of intervals in the x direction, must be int', required=True )
parser.add_argument('--ny', type=int, help='Number of intervals in the y direction, must be int', required=True )
parser.add_argument('--nz', type=int, help='Number of intervals  in the z direction, must be int', required=True )
parser.add_argument('--Relx', type=float, help = 'Length of the X range OR min and max values of the X range relative to the nominal center, must be float', 
                    nargs='+', required=True)
parser.add_argument('--Rely', type=float, help='Length of the Y range OR min and max values of the Y range relative to the nominal center, must be float', 
                    nargs='+', required=True)
parser.add_argument('--Relz', type=float, help='Length of the Z range OR min and max values of the Z range relative to the nominal center, must be float', 
                    nargs='+', required=True)
parser.add_argument('--CTTime', type=float, help='Time delay between scans (s), must be float', default=.2)
parser.add_argument('--output_path', type=dir_path, help='Directory path to save the file to', default=False)

args=parser.parse_args()


# =============================================================================
# GENERATE LASER PTS
# =============================================================================

# ****Relx ranges****
if (len(args.Relx)>2):
    sys.exit("Relx only takes two numbers")
   
elif (len(args.Relx)==2):
    xm = np.linspace(args.Relx[0], args.Relx[1], args.nx+1) + args.x
    
elif (len(args.Relx)==1):
    xm = np.linspace(-args.Relx[0], args.Relx[0], args.nx+1 ) + args.x
    
# ****Rely ranges****
if (len(args.Rely)>2):
    sys.exit("Rely only takes two numbers")
   

elif (len(args.Rely)==2):
    ym = np.linspace(args.Rely[0], args.Rely[1], args.ny+1 ) + args.y
    
elif (len(args.Rely)==1):
    ym = np.linspace(-args.Rely[0], args.Rely[0], args.ny+1 ) + args.y
 
# ****Relz ranges**** 
if (len(args.Relz)>2):
    sys.exit("Relz only takes two numbers")
   
elif (len(args.Relz)==2):
    zm = np.linspace(args.Relz[0], args.Relz[1], args.nz+1 ) + args.z
    
elif (len(args.Relz)==1):
    zm = np.linspace(-args.Relz[0], args.Relz[0], args.nz+1 ) + args.z
    

xyz=[xm,ym,zm]
coords = {'x':0, 'y':1, 'z':2}

# order from left to right is fastest to slowest (xyz)
# [0] position is fastest and [2] position is slowest
# np.meshgrid(slowest, medium, fastest, indexting='ij)
if (args.order=='xyz'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    xx = smf[2].flatten() # flattened fast array  
    yy = smf[1].flatten() # flattened medium array
    zz = smf[0].flatten() # flattened slow array 
    
elif (args.order == 'zyx'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    zz = smf[2].flatten() # flattened fast array  
    yy = smf[1].flatten() # flattened medium array
    xx = smf[0].flatten() # flattened slow array    
 
elif (args.order == 'yzx'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    yy = smf[2].flatten() # flattened fast array  
    zz = smf[1].flatten() # flattened medium array
    xx = smf[0].flatten() # flattened slow array 
    
elif (args.order == 'xzy'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    xx = smf[2].flatten() # flattened fast array  
    zz = smf[1].flatten() # flattened medium array
    yy = smf[0].flatten() # flattened slow array     
    
elif (args.order == 'zxy'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    zz = smf[2].flatten() # flattened fast array  
    xx = smf[1].flatten() # flattened medium array
    yy = smf[0].flatten() # flattened slow array 

elif (args.order == 'yxz'):
    smf = np.meshgrid(xyz[coords[args.order[2]]], xyz[coords[args.order[1]]], xyz[coords[args.order[0]]], indexing='ij')
    yy = smf[2].flatten() # flattened fast array  
    xx = smf[1].flatten() # flattened medium array
    zz = smf[0].flatten() # flattened slow array     

XYZ1 = np.vstack( (xx, yy, zz) ).T
# Move this section up so save_file_f/b don't complain.
XYZAll = XYZ1.copy()

NumPts = XYZAll.shape[0]
Time = args.CTTime * NumPts / 3600
print('NumPts:', NumPts)
print( 'Time', (0.5 + args.CTTime) * NumPts / 60, 'min' ) # Time Estimate

# saving the front plate data
def save_file_f():
    """ Saving the OuputForSpec2R data into a file for the front side of the plate """
    ff=asksaveasfilename(initialfile = "front.txt", defaultextension='.txt', 
                    filetypes=[("All Files", "*.*"), ("Text Documents", ".txt")], 
                    initialdir=args.output_path)
    OuputForSpec2R(ff, XYZAll, args.CTTime, (args.ome), 999 )


# saving the back plate data 
def save_file_b():
    """ Saving the OuputForSpec2R data into a file for the back side of the plate """
    fb=asksaveasfilename(initialfile = "back.txt", defaultextension='.txt', 
                    filetypes=[("All Files", "*.*"), ("Text Documents", ".txt")],
                    initialdir=args.output_path)
    OuputForSpec2R(fb, XYZAll, args.CTTime, (args.ome+180), 999 ) 

# =============================================================================
# PLOT LASER PTS
# =============================================================================

# Create matplotlib figure & axes
fig, axs = plt.subplots(1, 3)

# Set a figure title
fig.suptitle('Points to Scan')

# Plot the points to scan
axs[0].plot(XYZ1[:,0], XYZ1[:,1], '.')
axs[0].set_xlabel('X')
axs[0].set_ylabel('Y')

axs[1].plot(XYZ1[:,1], XYZ1[:,2], '.')
axs[1].set_xlabel('Y')
axs[1].set_ylabel('Z')

axs[2].plot(XYZ1[:,0], XYZ1[:,2], '.')
axs[2].set_xlabel('X')
axs[2].set_ylabel('Z')

fig.tight_layout()
plt.subplots_adjust(bottom=0.25)

# Create buttons & their axes.
save_front_b = Button(plt.axes([0.75, 0.05, 0.2, 0.075]), 'Save Front Plate')
save_back_b = Button(plt.axes([0.5, 0.05, 0.2, 0.075]), 'Save Back Plate')


# Define a callback function to run when the buttons are pressed
def save_file_mpl(event, plate_side):
    root = Tk()
    root.withdraw()
    if plate_side == 'front':
        save_file_f()
    elif plate_side == 'back':
        save_file_b()
    root.destroy()

# Bind the buttons to the callback function above
cid_save_front = save_front_b.on_clicked(lambda event: save_file_mpl(event, 'front'))
cid_save_back = save_back_b.on_clicked(lambda event: save_file_mpl(event, 'back'))

plt.show(block=True)

save_front_b.disconnect(cid_save_front)
save_back_b.disconnect(cid_save_back)
 



